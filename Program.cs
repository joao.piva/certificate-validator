﻿using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace CertificateValidator
{
    internal class Program
    {
       
        static async Task Main(string[] args)
        {
            string url = "app.hub2b.com.br";

            // definimos o callback pra retornar "true" sempre pra aceitar qualquer certificado, justamente pq queremos verificar se é válido ou não
            RemoteCertificateValidationCallback certCallback = (_, _, _, _) => true;

            using var client = new TcpClient(url, 443);
            using var sslStream = new SslStream(client.GetStream(), true, certCallback);

            await sslStream.AuthenticateAsClientAsync(url);

            var serverCertificate = sslStream.RemoteCertificate;

            if (serverCertificate != null)
            {
                var certificate = new X509Certificate2(serverCertificate);

                await Console.Out.WriteLineAsync($"{url} - Data de validade: {certificate.GetExpirationDateString()}");
            }
            else
            {
                Console.WriteLine($"{url} - Certificado retornou nulo");
            }
        }
    }
}